# Copy Far "AI"

Copy Far "AI" - uma licença próxima do [copyleft][], mas longe das ditas
"Inteligências Artificiais".

[copyleft]: https://pt.wikipedia.org/wiki/Copyleft

## A licença base

Use esta licença e adapte-a conforme necessário:

> {{ config.site_name }} - v0.0.1 - {{ slogan.pt }} -
> {{ base_url }}/pt
>
> {{ licenses.v0_0_1.pt }} `$license`

Onde `$license` é qualquer outra licença, como [CC BY 4.0][].

[CC BY 4.0]: https://creativecommons.org/licenses/by/4.0/deed.pt_BR
