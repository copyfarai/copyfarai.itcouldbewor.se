# Efetividade

## Validade

No campo legal, a validade de uma licença restritiva às "IAs" pode ser decidida
nos tribubais. Consulte a legislação de cada país para mais informações.

De um ponto de vista ético/moral/político, no entando, esta licença pode ser
útil, independente do de como a questão do "uso justo" for decidida nas cortes
ou da validade da licença em quaisquer jurisdições.

## Aplicabilidade

É discutível o quanto operadores de "Inteligências Artificiais" respeitam
este tipo de licenciamento, especialmente no caso de algoritmos e conjuntos
de dados fechados, onde se torna inviável uma auditoria buscando violações
de licenças.

Ainda assim, usar um sistema de licenciamento do tipo {{ config.site_name }}
é uma forma de marcar posição, expor o problema e responsabilizá-los.
