# Effectiveness

## Validity

In the legal stage, the validity of an "AI"-restrictive license may be settled 
on courts. Check the existing legislation each country for more information.

From an ethical/moral/political standpoint, though, this license might be
useful, regardless of the outcome for the "fair use" question or the license's
validity in any jurisdiction.

## Applicability

It's a debate whether "Artificial Intelligence" operators respect such type of
licensing, especially for closed algorithms and datasets, where audits looking
for license violations are unfeasible.

Even though, using a licensing system such as {{ config.site_name }} is a way
to make an statement, expose the issue and make them accountable.
