# Copy Far "AI"

Copy Far "AI" - A license close to [copyleft][], but far from the so-called
"Artificial Intelligences".

[copyleft]: https://en.wikipedia.org/wiki/Copyleft

## The base license

Use and adapt this license as necessary:

> {{ config.site_name }} - v0.0.1 - {{ slogan.en }} -
> {{ base_url }}
>
> {{ licenses.v0_0_1.en }} `$license`

Where `$license` is any other license, such as [CC BY 4.0][].

[CC BY 4.0]: https://creativecommons.org/licenses/by/4.0/
