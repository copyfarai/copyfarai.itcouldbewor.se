# Contributing

How to help:

* [x] [Using](../how/README.md) the licenses!
* [x] Promoting the project.
* [x] Adopting or creating [tasks][], such as suggesting changes in the base license text.

[tasks]: https://0xacab.org/copyfarai/copyfarai.itcouldbewor.se/-/issues
