# Contribuindo

Como colaborar:

* [x] [Usando](../how/README.md) as licenças!
* [x] Divulgando o projeto.
* [x] Adotando ou criando [tarefas][], como por exemplo sugerindo mudanças no
      texto base da licença.

[tarefas]: https://0xacab.org/copyfarai/copyfarai.itcouldbewor.se/-/issues
