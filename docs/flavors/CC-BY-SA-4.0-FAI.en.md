# CC BY-SA 4.0 FAI

> {{ config.site_name }} - v0.0.1 - {{ slogan.en }} -
> {{ base_url }}/flavors/CC-BY-SA-4.0-FAI/
>
> {{ licenses.v0_0_1.en }} Creative Commons Attribution-ShareAlike 4.0
> International ([CC BY-SA 4.0][]).

[CC BY-SA 4.0]: https://creativecommons.org/licenses/by-sa/4.0/
