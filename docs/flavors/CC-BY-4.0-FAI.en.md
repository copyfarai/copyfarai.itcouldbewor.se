# CC BY 4.0 FAI

> {{ config.site_name }} - v0.0.1 - {{ slogan.en }} -
> {{ base_url }}/flavors/CC-BY-4.0-FAI/
>
> {{ licenses.v0_0_1.en }} Creative Commons Attribution 4.0 International ([CC
> BY 4.0][]).

[CC BY 4.0]: https://creativecommons.org/licenses/by/4.0/
