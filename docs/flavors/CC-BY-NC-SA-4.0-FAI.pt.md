# CC BY-NC-SA 4.0 FAI

> {{ config.site_name }} - v0.0.1 - {{ slogan.pt }} -
> {{ base_url }}/pt/flavors/CC-BY-NC-SA-4.0-FAI
>
> {{ licenses.v0_0_1.pt }} Creative Commons
> Atribuição-NãoComercial-CompartilhaIgual 4.0 Internacional ([CC BY-NC-SA 4.0][]).

[CC BY-NC-SA 4.0]: https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR
