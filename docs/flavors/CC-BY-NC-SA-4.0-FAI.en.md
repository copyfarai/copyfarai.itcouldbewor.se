# CC BY-NC-SA 4.0 FAI

> {{ config.site_name }} - v0.0.1 - {{ slogan.en }} -
> {{ base_url }}/flavors/CC-BY-NC-SA-4.0-FAI/
>
> {{ licenses.v0_0_1.en }} Creative Commons
> Attribution-NonCommercial-ShareAlike 4.0 International ([CC BY-NC-SA 4.0][]).

[CC BY-NC-SA 4.0]: https://creativecommons.org/licenses/by-nc-sa/4.0/
