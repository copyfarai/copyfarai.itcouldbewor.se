# Contact

You can get in touch:

* Filling an [issue][].
* Sending an e-mail message to rhatto at riseup.net.

[issue]: https://0xacab.org/copyfarai/copyfarai.itcouldbewor.se/-/issues
