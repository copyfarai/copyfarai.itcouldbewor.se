# Why

> So you can easily state that your contend production is not automatically
> available for "Artificial Intelligences" without previous authorization.

This project was created to supply the absence of specific licenses that
restricts ingestion of content in systems such as [LLMs][] and image classifiers.

Besides existing [discussions](../references/README.md) about non-licensing for
[LLMs][] and such, there was no specific and easily usable license.

Then, a disclaimer was created to be plugged in any existing license.

[LLMs]: https://en.wikipedia.org/wiki/Large_language_model
