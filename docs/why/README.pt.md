# Motivação

> Para que você possa indicar facilmente que sua produção de conteúdo não está
> automaticamente disponível para "Inteligências Artificiais" sem a prévia
> autorização.

Este projeto supre a ausência de licenças específicas que restrinjam a ingestão
de obras em determinados sistemas, como [LLMs][] e classificadores de imagem.

Apesar de existirem [discussões](../references/README.md) sobre
não-licenciamento de conteúdo para [LLMs][] e afins, não havia uma licença
específica e facilmente usável.

Por isso, um disclaimer foi criado para se encaixar em qualquer licença
existente.

[LLMs]: https://en.wikipedia.org/wiki/Large_language_model
