# Referências

Alguns artigos e discussões sobre o assunto.

## "IA" e licenciamento

* [Artificial intelligence and copyright - Wikipedia](https://en.wikipedia.org/wiki/Artificial_intelligence_and_copyright)
* [GitHub - non-ai-licenses/non-ai-licenses: This repository contains software licenses that restrict software from being used in AI training datasets or AI technologies.](https://github.com/non-ai-licenses/non-ai-licenses)
* [Is there a license that explicitly exempts AI from all conditions? - Open Source Stack Exchange](https://opensource.stackexchange.com/questions/13932/is-there-a-license-that-explicitly-exempts-ai-from-all-conditions)
* [Is there a license like MIT that explicitly forbids the use of AI? - Open Source Stack Exchange](https://opensource.stackexchange.com/questions/13920/is-there-a-license-like-mit-that-explicitly-forbids-the-use-of-ai)
* [First Open Source Copyright Lawsuit Challenges GitHub Copilot](https://www.infoq.com/news/2022/11/lawsuit-github-copilot/)
* [relicensing - How can I protect the code from being 'rephrased' by AI to avoid license limitations? - Open Source Stack Exchange](https://opensource.stackexchange.com/questions/13860/how-can-i-protect-the-code-from-being-rephrased-by-ai-to-avoid-license-limitat/13878)

## "IA" e violações de copyright

* [Copyright exceptions for AI training data—will there be an international level playing field? | Journal of Intellectual Property Law & Practice | Oxford Academic](https://academic.oup.com/jiplp/article/17/12/973/6880991?login=false)
* [Generative AI Has an Intellectual Property Problem](https://hbr.org/2023/04/generative-ai-has-an-intellectual-property-problem)
* [The lawsuit against Microsoft, GitHub and OpenAI that could change the rules of AI copyright - The Verge](https://www.theverge.com/2022/11/8/23446821/microsoft-openai-github-copilot-class-action-lawsuit-ai-copyright-violation-training-data)
* [The scary truth about AI copyright is nobody knows what will happen next - The Verge](https://www.theverge.com/23444685/generative-ai-copyright-infringement-legal-fair-use-training-data)
* [Reported EU legislation to disclose AI training data could trigger copyright lawsuits - The Verge](https://www.theverge.com/2023/4/28/23702437/eu-ai-act-disclose-copyright-training-data-report)
* [These artists found out their work was used to train AI. Now they're furious | CNN Business](https://www.cnn.com/2022/10/21/tech/artists-ai-images/index.html)

## Casos similares the restrição de uso

* [Copyfarleft](https://wiki.p2pfoundation.net/Copyfarleft),
  [Copyfair](https://wiki.p2pfoundation.net/Copyfair) e [outras
  licenças](https://wiki.p2pfoundation.net/Category:Licensing):
    * [LibreCybernetics/awesome-copyfarleft](https://github.com/LibreCybernetics/awesome-copyfarleft/)
    * [Em defesa do software livre – Código Urbano](https://www.codigourbano.org/em-defesa-do-software-livre/)
    * [Autorias em contexto: estudos antropológicos sobre criação e propriedade](http://hdl.handle.net/1843/33430)
* [licensing - "Open-source" licenses that explicitly prohibit military applications - Software Engineering Stack Exchange](https://softwareengineering.stackexchange.com/questions/199055/open-source-licenses-that-explicitly-prohibit-military-applications)
* [FAIR Principles - GO FAIR](https://www.go-fair.org/fair-principles/)
* [CARE Principles — Global Indigenous Data Alliance](https://www.gida-global.org/care):
    * [Operationalizing the CARE and FAIR Principles for Indigenous data futures | Scientific Data](https://www.nature.com/articles/s41597-021-00892-0)
