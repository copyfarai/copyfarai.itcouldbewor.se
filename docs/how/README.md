# How to use

{{ config.site_name }} is a license shell to wrap other licenses.

1. Choose a license for your content. Any existing license that you're allowed to use.
2. Include the [base Copy Far "AI" text][] in the content to be licensed, right
   before the text of the chosen license.
3. Publish your content!

[base Copy Far "AI" text]: ../README.md

## Example

1. Say you have chosen the [CC BY 4.0][] license.
2. Include the [base Copy Far "AI" text][] right before the [CC BY 4.0][] license, like [here][].
3. Publish your content!

[CC BY 4.0]: https://creativecommons.org/licenses/by/4.0/
[here]: ../flavors/CC-BY-4.0-FAI.md

Some license "flavors" are ready to use, available at the "Flavors" menu item.
