# Como usar

{{ config.site_name }} é uma licença de uso de conteúdo to tipo "casca", usada
para embalar outras licenças.

1. Escolha uma licença de uso de conteúdo. Qualquer licença que você possa usar.
2. Inclua o [texto base da Copy Far "AI"][] no conteúdo a ser licenciado, e
   exatamente antes do texto da licença escolhida.
3. Publique seu conteúdo!

[texto base da Copy Far "AI"]: ../README.md

## Exemplo

1. Suponha que você tenha escolhido a licença [CC BY 4.0][].
2. Inclua o [texto base da Copy Far "AI"][] exatamente antes da licença [CC BY
   4.0][], como [aqui][].
3. Publique seu conteúdo!

[CC BY 4.0]: https://creativecommons.org/licenses/by/4.0/deed.pt_BR
[aqui]: ../flavors/CC-BY-4.0-FAI.md

Alguns "sabores" de licenças já estão prontos para usar, acessíveis no item de
menu "Sabores".
